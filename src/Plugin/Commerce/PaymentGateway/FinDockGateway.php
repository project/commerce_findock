<?php

namespace Drupal\commerce_findock\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\HasPaymentInstructionsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the FinDockGateway Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_findock",
 *   label = "FinDock",
 *   display_label = "FinDock",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_findock\PluginForm\OffsiteRedirect\FinDockForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 * )
 */
class FinDockGateway extends OffsitePaymentGatewayBase implements HasPaymentInstructionsInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Salesforce auth plugin manager.
   *
   * @var \Drupal\salesforce\SalesforceAuthProviderPluginManagerInterface
   */
  protected $salesForceAuthPluginManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The SalesForce API client.
   *
   * @var \Drupal\salesforce\Rest\RestClientInterface
   */
  protected $salesforceClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $gateway */
    $gateway = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $gateway->httpClient = $container->get('http_client');
    $gateway->token = $container->get('token');
    $gateway->salesForceAuthPluginManager = $container->get('plugin.manager.salesforce.auth_providers');
    $gateway->moduleHandler = $container->get('module_handler');
    $gateway->salesforceClient = $container->get('salesforce.client');
    return $gateway;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'payment_method' => '',
        'processor' => NULL,
        'source_connector' => '',
        'findock_target' => '',
        'instructions' => [
          'value' => '',
          'format' => 'plain_text',
        ],
        'limit' => 0,
        'window' => 0,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    try {
      $payment_methods = $this->salesforceClient->apiCall('/services/apexrest/cpm/v2/PaymentMethods');
      $source_connectors = $this->salesforceClient->apiCall('/services/apexrest/cpm/v2/SourceConnectors');
    }
    catch (\Exception $e) {
      $this->messenger()->addError('Failed to connect to salesforce, ensure that the authentication setup is correct. Error: @error', ['@error' => $e->getMessage()]);
      return $form;
    }

    $payment_method_options = [];
    foreach ($payment_methods['PaymentMethods'] as $payment_method) {
      $payment_method_options[$payment_method['Name']] = [];
      foreach ($payment_method['Processors'] as $processor) {
        $payment_method_options[$payment_method['Name']][$payment_method['Name'] . ':' . $processor['Name']] = $processor['Name'];
      }
    }

    $form['payment_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Payment Method & processor'),
      '#options' => $payment_method_options,
      '#default_value' => $this->configuration['payment_method'] . ':' . $this->configuration['processor'],
      '#required' => TRUE,
    ];

    $source_connector_options = [];
    foreach ($source_connectors['SourceConnectors'] as $source_connector) {
      $source_connector_options[$source_connector['Name']] = $source_connector['PrettyName'];
    }

    $form['source_connector'] = [
      '#type' => 'select',
      '#title' => $this->t('Source Connector'),
      '#options' => $source_connector_options,
      '#default_value' => $this->configuration['source_connector'],
      '#required' => TRUE,
    ];

    $form['findock_target'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Target'),
      '#default_value' => $this->configuration['findock_target'],
      ];

    $form['instructions'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Payment instructions'),
      '#description' => $this->t('Shown the end of checkout, after the customer has placed their order.'),
      '#default_value' => $this->configuration['instructions']['value'],
      '#format' => $this->configuration['instructions']['format'],
      '#element_validate' => ['token_element_validate'],
      '#token_types' => ['commerce_order', 'commerce_payment'],
    ];
    $form['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['commerce_order', 'commerce_payment'],
    ];

    $form['flood_protection'] = [
      '#type' => 'details',
      '#title' => $this->t('Flood protection'),
      '#open' => TRUE,
      '#description' => $this->t('The flood protection limits the number of allowed requests per ip and per order. Setting both values to 0 disables the flood protection.'),
    ];

    $form['flood_protection']['limit'] = [
      '#type' => 'number',
      '#title' => 'Limit',
      '#description' => $this->t('How many retries are allowed per ip and order in the specified time window.'),
      '#min' => 0,
      '#default_value' => $this->configuration['limit'],
    ];

    $form['flood_protection']['window'] = [
      '#type' => 'number',
      '#title' => 'Window (in seconds)',
      '#description' => $this->t('The time window for retries in seconds.'),
      '#min' => 0,
      '#default_value' => $this->configuration['window'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      [$payment_method, $processor] = \explode(':', $values['payment_method']);
      $this->configuration['payment_method'] = $payment_method;
      $this->configuration['processor'] = $processor;
      $this->configuration['source_connector'] = $values['source_connector'];
      $this->configuration['findock_target'] = $values['findock_target'];
      $this->configuration['limit'] = $values['flood_protection']['limit'];
      $this->configuration['window'] = $values['flood_protection']['window'];
      $this->configuration['instructions'] = $values['instructions'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentInstructions(PaymentInterface $payment) {
    $instructions = [];
    if (!empty($this->configuration['instructions']['value'])) {
      $instructions_text = $this->token->replace($this->configuration['instructions']['value'], [
        'commerce_order' => $payment->getOrder(),
        'commerce_payment' => $payment,
      ]);

      $instructions = [
        '#type' => 'processed_text',
        '#text' => $instructions_text,
        '#format' => $this->configuration['instructions']['format'],
      ];
    }

    return $instructions;
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {

    try {

      $token = $this->salesForceAuthPluginManager->getToken();

      // We can only verify a payment if there is an installment ID.
      if ($order->getData('findock_installment_id')) {
        $response = $this->httpClient->get(
          $this->configuration['url'] . '/' . $order->getData('findock_installment_id'), [
          'headers' => [
            'Authorization' => 'Bearer ' . $token->getAccessToken(),
            'Content-Type' => 'application/json',
          ],
        ]
        );
        $response_data = Json::decode((string) $response->getBody());

        if (\Drupal::moduleHandler()->moduleExists('past')) {
          past_event_save('commerce_findock', 'installment_data', 'Installment data #' . $order->id() . '(Installment #' . $order->getData('findock_installment_id') . ')', ['data' => $response_data]);
        }
      }

      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment = $payment_storage->create([
        'state' => 'authorization',
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->parentEntity->id(),
        'order_id' => $order->id(),
        'remote_id' => $order->getData('findock_installment_id'),
        'remote_state' => 'pending',
      ]);
      $payment->save();
    }
    catch (\Exception $e) {
      throw new PaymentGatewayException($this->t('Failed to validate payment: @message', ['@message' => $e->getMessage()]), $e->getCode(), $e);
    }

  }

}
