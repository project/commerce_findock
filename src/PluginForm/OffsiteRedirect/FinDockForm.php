<?php

namespace Drupal\commerce_findock\PluginForm\OffsiteRedirect;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\salesforce\Rest\RestException;
use Drupal\salesforce\SalesforceAuthProviderPluginManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Payment redirect form for FinDock.
 */
class FinDockForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Salesforce auth plugin manager.
   *
   * @var \Drupal\salesforce\SalesforceAuthProviderPluginManagerInterface
   */
  protected $salesForceAuthPluginManager;

  /**
   * Flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected FloodInterface $flood;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * FinDockForm constructor.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\salesforce\SalesforceAuthProviderPluginManagerInterface $salesforce_auth_plugin_manager
   *   The salesforce auth plugin manager.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(Client $http_client, ModuleHandlerInterface $module_handler, SalesforceAuthProviderPluginManagerInterface $salesforce_auth_plugin_manager, FloodInterface $flood, MessengerInterface $messenger) {
    $this->httpClient = $http_client;
    $this->moduleHandler = $module_handler;
    $this->salesForceAuthPluginManager = $salesforce_auth_plugin_manager;
    $this->flood = $flood;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('module_handler'),
      $container->get('plugin.manager.salesforce.auth_providers'),
      $container->get('flood'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    if ($configuration['limit'] > 0 && $configuration['window'] > 0 ) {
      $order = $payment->getOrder();

      // Per IP limit.
      if (!$this->flood->isAllowed('commerce_findock.gateway_payment', $configuration['limit'], $configuration['window'])) {
        $this->messenger->addError($this->t('Too many failed payment attempts from your IP address. This IP address is temporarily blocked. Try again later.'));
        throw new PaymentGatewayException($this->t('A request for FinDock was blocked due to many attempts from the same IP.'));
      }
      $this->flood->register('commerce_findock.gateway_payment', $configuration['window']);

      // Per order limit.
      if (!$this->flood->isAllowed('commerce_findock.gateway_payment', $configuration['limit'], $configuration['window'], 'order-' . $order->id())) {
        $this->messenger->addError($this->t('Too many failed payment attempts for this order. Payments are temporarily blocked. Try again later.'));
        throw new PaymentGatewayException($this->t('A request for FinDock was blocked due to many attempts for the same order.'));
      }
      $this->flood->register('commerce_findock.gateway_payment', $configuration['window'], 'order-' . $order->id());
    }

    $order = $payment->getOrder();
    $profile = $order->getBillingProfile();

    // @todo Make these settings configurable.
    $data = [
      'SuccessURL' => $form['#return_url'],
      'FailureURL' => $form['#cancel_url'],
      'Origin' => $payment->getPaymentGateway()->label(),
      'Payer' => [],
      'OneTime' => [
        'Amount' => $payment->getAmount()->getNumber(),
        'CurrencyISOCode' => $payment->getAmount()->getCurrencyCode(),
      ],
      'PaymentMethod' => [
        'Name' => $configuration['payment_method'],
        'Processor' => $configuration['processor'] ?: NULL,
        'Target' => $configuration['findock_target'] ?: NULL,
      ],
      'Settings' => [
        'SourceConnector' => $configuration['source_connector'],
      ]
    ];

    if ($profile->hasField('address') && !$profile->get('address')->isEmpty()) {
      $data['Payer'] += [
        'Contact' => [
          'SalesforceFields' => [
            'FirstName' => $profile->get('address')->given_name,
            'LastName' => $profile->get('address')->family_name,
            'MailingStreet' => $profile->get('address')->address_line1,
            'MailingCity' => $profile->get('address')->locality,
            'MailingPostalCode' => $profile->get('address')->postal_code,
            'MailingCountry' => $profile->get('address')->country_code,
          ],
        ],
        'Account' => [
          'SalesforceFields' => [
            'BillingStreet' => $profile->get('address')->address_line1,
            'BillingCity' => $profile->get('address')->locality,
            'BillingPostalCode' => $profile->get('address')->postal_code,
            'BillingCountry' => $profile->get('address')->country_code,
          ],
        ],
      ];
    }

    $this->moduleHandler->alter('commerce_findock_data', $data, $order, $payment);

    $event = NULL;
    if ($this->moduleHandler->moduleExists('past')) {
      $event = past_event_create('commerce_findock', 'payment_data', 'Payment #' . $order->id());
      $event->addArgument('data', json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
      $event->addArgument('order', $order);
      $event->addArgument('payment', $payment);
    }

    try {

      $token = $this->salesForceAuthPluginManager->getToken() ?? $this->salesForceAuthPluginManager->refreshToken();
      if (empty($token)) {
        throw new \Exception("Could not retrieve Auth Token. Is there a default salesforce auth provider set up?");
      }

      $start = microtime(TRUE);

      /** @var \Drupal\salesforce\Rest\RestClientInterface $client */
      $client = \Drupal::service('salesforce.client');

      /** @var \Drupal\salesforce\Rest\RestResponse $response */
      $response = $client->apiCall('/services/apexrest/cpm/v2/PaymentIntent', $data, 'POST', TRUE);

      $time = round((microtime(TRUE) - $start) * 1000, 2);
      if ($event) {
        $event->addArgument('time', $time);
      }

      if ($response->getStatusCode() != 200) {
        throw new \Exception($response->getReasonPhrase(), $response->getStatusCode());
      }

      $response_data = Json::decode((string) $response->getBody());
      if ($event) {
        $event->addArgument('response_data', $response_data);
      }
      $order->setData('payment_intent_id', $response_data['Id']);

      // If there is no redirect URL then the configured payment method
      // does not need additional processing. Create the payment and continue.
      if (empty($response_data['RedirectURL'])) {

        // Save the payment and directly redirect to the next step.
        $payment->setState('authorization');
        $payment->setRemoteState('pending');
        $payment->setRemoteId($order->getData('payment_intent_id'));
        $payment->save();

        if (\Drupal::moduleHandler()->moduleExists('past')) {
          past_event_save('commerce_findock', 'installment_data', 'Installment data #' . $order->id() . '(Installment #' . $order->getData('findock_installment_id') . ')', ['data' => $response_data, 'payment' => $payment]);
        }

        /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
        $checkout_flow = $order->get('checkout_flow')->entity;
        $checkout_flow_plugin = $checkout_flow->getPlugin();

        $next_step_id = $checkout_flow_plugin->getNextStepId($order->get('checkout_step')->value);
        $checkout_flow_plugin->redirectToStep($next_step_id);
      }
      else {
        $redirect_url = $response_data['RedirectURL'];
        $order->save();
      }
    }
    catch (NeedsRedirectException $e) {
      throw $e;
    }
    catch (RestException $e) {
      if ($event) {
        $event->addException($e);
        $event->addArgument('response', (string) $e->getResponse()->getBody());
      }
      throw new PaymentGatewayException(t('Failed to initialize payment: @error', ['@error' => $e->getMessage()]), $e->getCode(), $e);
    }
    catch (\Exception $e) {
      if ($event) {
        $event->addException($e);
      }
      throw new PaymentGatewayException(t('Failed to initialize payment: @error', ['@error' => $e->getMessage()]), $e->getCode(), $e);
    }
    finally {
      if ($event) {
        $event->save();
      }
    }

    if (!empty($redirect_url)) {
      $form = $this->buildRedirectForm($form, $form_state, $redirect_url, []);
    }
    return $form;
  }

}
