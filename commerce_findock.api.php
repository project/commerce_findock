<?php

/**
 * @file
 * Hook documentation for Commerce FinDock.
 */

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Allows to alter the data that is sent to FinDock.
 *
 * @param array $data
 *   The data array that is sent out.
 * @param \Drupal\commerce_order\Entity\OrderInterface $order
 *   The order.
 * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
 *   The payment.
 *
 * @see \Drupal\commerce_findock\PluginForm\OffsiteRedirect\FinDockForm::buildConfigurationForm()
 */
function hook_commerce_findock_data_alter(array &$data, OrderInterface $order, PaymentInterface $payment) {
  $profile = $order->getBillingProfile();

  // Set the account record type and account fields.
  if ($profile->get('field_is_organization')->value) {
    $data['Payer']['AccountRecordTypeName'] = 'Organization';
    $data['Payer']['Account']['SalesforceFields']['Name'] = $profile->get('field_organization')->value;
  }

  // Set extra contact fields.
  $data['Payer']['Contact']['SalesforceFields']['ContactField'] = $profile->get('field_custom')->value;

  // Set custom fields on the payment level.
  $data['OneTime']['CustomFields']['cpm__Originating_Campaign__c'] = 123456;

  // Define a recurring payment.
  $data['Recurring'] = $data['Payment'];
  // Other intervals: Quarterly, Half-Year, Yearly.
  $data['Recurring']['Frequency'] = 'Monthly';

  // Set start date, with optional offset.
  $start_date = new DrupalDateTime();
  $start_date = $start_date->add(new DateInterval('P1M'));
  $data['Recurring']['StartDate'] = $start_date->format('Y-m-d');

  // No initial payment.
  unset($data['OneTime']);

  // Special parameters.
  if ($data['PaymentMethod']['Name'] == 'PayPal') {
    $data['PaymentMethod']['Parameters']['foo'] = 'bar';
  }

}
